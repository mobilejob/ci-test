#!/bin/bash

# to check the outcome of this script call: "./tests.sh; echo $?"

# play with the database from the container:
# mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p"$MYSQL_ENV_MYSQL_ROOT_PASSWORD"

# create some schema
MYSQL_OPTIONS='-B -h ::1 --user=root --password=password database' # bitbucket pipline service mysql exposes on localhost
mysql $MYSQL_OPTIONS -e 'DROP TABLE IF EXISTS things; CREATE TABLE things ( id INT, name VARCHAR(20) ); INSERT INTO things (id, name) VALUES (1, "Manfred"); INSERT INTO things (id, name) VALUES (1, "Ice Cream");'

# test something
result=$(mysql -N $MYSQL_OPTIONS -e 'SELECT name FROM things where id = 1')
if [[ "$result" == *"Manfred"* ]]; then
  echo "tests passed"
  exit 0
else
  echo "tests failed"
  exit 1
fi
