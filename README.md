# CI trials

We assume that you are familiar with vagrant.

```bash
# bootstrap the vagrant box
vagrant box update
vagrant up

# install docker
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo usermod -aG docker ${USER}
# log out and back in
```

## Test pipeline locally

```bash
cd /vagrant
# start database (image and environment variables should match bitbucket-pipelines.yml)
docker run --name mysql-service -e MYSQL_DATABASE=database -e MYSQL_ROOT_PASSWORD=password -d mysql:5.7
docker rm -f mysql-service

# start app (image should match bitbucket-pipelines.yml)
docker run --name app -it --volume=$(pwd):/localDebugRepo --workdir="/localDebugRepo" --network="container:mysql-service" --entrypoint=/bin/bash ruby:2.1
# ...now run the script steps in the bitbucket-pipelines.yml
docker rm app # remove the container
```

## Resources

- [BitBucket Pipeline - How to test locally](https://confluence.atlassian.com/bitbucket/debug-your-pipelines-locally-with-docker-838273569.html)
- [BitBucket Pipeline - Deploy Examples](https://bitbucket.org/account/user/awslabs/projects/BP)
- [Intro to Docker & Vagrant](https://flurdy.com/docs/docker/docker_osx_ubuntu.html)